package com.rogg.iplogger.web.rest;


import com.rogg.iplogger.domain.History;
import com.rogg.iplogger.domain.Status;
import com.rogg.iplogger.domain.StatusDto;
import com.rogg.iplogger.repository.HistoryRepository;
import com.rogg.iplogger.repository.StatusRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.Calendar;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("iplogger/")
public class StatusResource {
    private final StatusRepository statusRepository;
    private final HistoryRepository historyRepository;


    @PostMapping("")
    public ResponseEntity<StatusDto> newStatus(@RequestBody StatusDto statusDto) {
        CreateHistoryEntry(statusDto);
        UpdateStatus(statusDto);
        Status status = new Status();
        return ResponseEntity.created(URI.create("iplogger/" + status.getId())).body(statusDto);
    }

    private void CreateHistoryEntry(StatusDto statusDto){
        History history = new History();
        history.setDate(Calendar.getInstance().getTime());
        history.setState(statusDto.getState());
        history.setServerIP(statusDto.getServerIP());
        historyRepository.save(history);

    }

    private void UpdateStatus(StatusDto statusDto){
        Status oldStatus = statusRepository.findStatusByServerIP(statusDto.getServerIP());
        if (oldStatus != null)
        {
            oldStatus.setDateUpdate(Calendar.getInstance().getTime());
            if (oldStatus.getState().equals(statusDto.getState())){
                //state did change - do something with it
                oldStatus.setState(statusDto.getState());
            } else {
                //state didnt change
            }
            statusRepository.save(oldStatus);
        } else
        {
            //no data for the server yet
            Status newStatus = new Status();
            newStatus.setDateUpdate(Calendar.getInstance().getTime());
            newStatus.setDateCreate(Calendar.getInstance().getTime());
            newStatus.setServerIP(statusDto.getServerIP());
            newStatus.setState(statusDto.getState());
            statusRepository.save(newStatus);
        }
    }
}

package com.rogg.iplogger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpLoggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(IpLoggerApplication.class, args);
    }

}

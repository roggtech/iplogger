package com.rogg.iplogger.repository;

import com.rogg.iplogger.domain.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository

public interface StatusRepository extends JpaRepository<Status,Long> {
    @Query("SELECT u FROM Status u WHERE u.serverIP = ?1")
    Status findStatusByServerIP(String serverIP);
}
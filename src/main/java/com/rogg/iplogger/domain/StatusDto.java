package com.rogg.iplogger.domain;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor

public class StatusDto implements Serializable {

    private String serverIP;
    private String state;

}
package com.rogg.iplogger.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class Status {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "serverIP", nullable = false, unique = true)
    private String serverIP;
    private String state;
    private Date dateUpdate;
    private Date dateCreate;




}
